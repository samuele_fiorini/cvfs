qwe
clc
% This is the demo used for l1l2 statistics function development

%% Synthetic problem parameters

n = 800;          % Number of data points
p = 200;          % Number of variables
k = 30;           % Number of variables with nonzero coefficient
amplitude = 3.5;  % Magnitude of nonzero coefficients
q = 0.20;         % Target false discovery rate (FDR)
sigma = 0.5;      % Noise variance

seed = [];

trueDiscoveries = @(S,Rbeta) sum(abs(Rbeta(S)) > 0);
FDP = @(S,Rbeta) sum(Rbeta(S) == 0) / max(1, length(S));
printSummary = @(S,Rbeta) fprintf(...
    ['%d discoveries (%d True, %d False)\n' ...
     'FDP = %2.2f%% (target FDR = %2.f%%)\n'], ...
    size(S,2), trueDiscoveries(S,Rbeta), size(S,2)-trueDiscoveries(S,Rbeta),100*FDP(S,Rbeta), 100*q);

[X, y, beta_star] = l1l2.utils.genSynthDataset('sparse', n, p, k, amplitude, sigma);

%% --- Function handlers:

% 1) feature selection
KF = @(Xw, yw) ...
    knockoff.filter(Xw, yw, q);
    
% 2) learning
RLS_fit = @(Xl,yl) ...
    CVFS.wrapper.CVRLS_fit(Xl,yl);
    
% 3) prediction
RLS_predict = @(Xp,betap) Xp*betap;

%% --- Test CVFS.modelSelection --- %

% Run model selection
[beta_hat, selected] = CVFS.modelSelection(X, y, KF, RLS_fit, RLS_predict, 5, 'modelSelection_debug');
printSummary(selected', beta_star)

