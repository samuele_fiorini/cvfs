qwe
clc
% This is the demo used for l1l2 statistics function development

%% Synthetic problem parameters

n = 600;          % Number of data points
p = 200;          % Number of variables
k = 30;           % Number of variables with nonzero coefficient
amplitude = 3.5;  % Magnitude of nonzero coefficients
q = 0.20;         % Target false discovery rate (FDR)
sigma = 0.5;      % Noise variance

seed = [];

[X, y, beta_star] = l1l2.utils.genSynthDataset('sparse', n, p, k, amplitude, sigma);

%% --- Function handlers:

% 1) feature selection
KF = @(Xw, yw) ...
    knockoff.filter(Xw, yw, q);
    
% 2) learning
RLS_fit = @(Xl,yl) ...
    CVFS.wrapper.CVRLS_fit(Xl,yl);
    
% 3) prediction
RLS_predict = @(Xp,betap) Xp*betap;

%% --- Test CVFS.KFold --- %

% Run CVFS
[FS,P] = CVFS.Kfold(X, y, KF, 5, 'Kfold_test', RLS_fit, RLS_predict);





