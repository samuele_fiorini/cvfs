% qwe
clc
%% Synthetic problem parameters

n = 600;         % Number of data points
p = 200;          % Number of variables
k = 30;           % Number of variables with nonzero coefficient

amplitude = 3.5;  % Magnitude of nonzero coefficients
sigma = 1;        % Noise variance
seed = [];

%% --- Function handlers:

% 1) feature selection
q = 0.20;  % Target false discovery rate (FDR)
KF = @(Xw, yw) ...
    knockoff.filter(Xw, yw, q, 'Statistic', @knockoff.stats.lassoSignedMax, ...
                               'Knockoffs', 'SDP','Threshold', 'knockoff');
                           
trueDiscoveries = @(S,Rbeta) sum(abs(Rbeta(S)) > 0);
FDP = @(S,Rbeta) sum(Rbeta(S) == 0) / max(1, length(S));
printSummary = @(S,Rbeta) fprintf(...
    ['%d discoveries (%d True, %d False)\n' ...
     'FDP = %2.2f%% (target FDR = %2.f%%)\n'], ...
    size(S,2), trueDiscoveries(S,Rbeta), size(S,2)-trueDiscoveries(S,Rbeta),100*FDP(S,Rbeta), 100*q);
power = @(GT,TD) TD/GT;

%% --- Tests:

%                 1       2         3             4                  5
dataset_names = {'null' 'sparse', 'correlated', 'blockcorrelated', 'zouhastie'};
dataset = dataset_names{2};
tag = strcat('CVFS_vs_KF_dummy','_',dataset);
debug = 1;
    
K = 600;
KF_FDP_array   = -ones(1,K);
KF_D_array     = -ones(1,K);
KF_TD_array    = -ones(1,K);
KF_FD_array    = -ones(1,K);
KF_POWER_array = -ones(1,K);

parfor i=1:K
    [X, y, beta_star] = l1l2.utils.genSynthDataset(dataset, n, p, k, amplitude, sigma);
    
    selected_kf = KF(X, y);
    
%     printSummary(selected_kf,beta_star);
    KF_FDP_array(i) = 100 * FDP(selected_kf,beta_star);
    KF_D_array(i) = size(selected_kf,2);
    KF_TD_array(i) = trueDiscoveries(selected_kf,beta_star);
    KF_FD_array(i) = KF_D_array(i) - KF_TD_array(i); 
    KF_POWER_array(i) = power(k,KF_TD_array(i));
end

%% --- results

fprintf('\n\navg(FDR)   = %2.3f\n',mean(KF_FDP_array))
fprintf('avg(Power) = %2.3f\n',mean(KF_POWER_array))