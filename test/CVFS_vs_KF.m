qwe
clc
% This demo aims at assessing wheter or not the feature selection strategy 
% called Knockoff filter benefits from cross-validation schemes.

%% Synthetic problem parameters

n = 600;          % Number of data points
p = 200;          % Number of variables
k = 30;           % Number of variables with nonzero coefficient
amplitude = 3.5;  % Magnitude of nonzero coefficients
sigma = 1;        % Noise variance

test_size = 0.2;  % External single split size
seed = [];        % Random seed

%% Knockoff filter parameters

Knockoffs_list = {'equi', 'SDP'};
Threshold_list = {'knockoff', 'knockoff+'};

Knockoffs = Knockoffs_list{2};
Threshold = Threshold_list{2};

%% --- Function handlers:

% 1) feature selection
q = 0.20; % Target false discovery rate (FDR)
B = 10;    % Number of splits 
% q = 1-(1-Q)^(1/B); % Sidak correction
% q = Q/B;           % Bonferroni correction
% q = Q;             % NO correction

KF = @(Xw, yw) ...
    knockoff.filter(Xw, yw, q, 'Statistic', @knockoff.stats.lassoSignedMax, ...
                               'Knockoffs', Knockoffs,'Threshold', Threshold);
% 2) learning
RLS_fit = @(Xl,yl) ...
    CVFS.wrapper.CVRLS_fit(Xl,yl);
    
% 3) prediction
RLS_predict = @(Xp,betap) Xp*betap;

%% --- Statistics print functions

trueDiscoveries = @(S,Rbeta) sum(abs(Rbeta(S)) > 0);
FDP = @(S,Rbeta) sum(Rbeta(S) == 0) / max(1, length(S));
power = @(GT,TD) TD/GT;
printSummary = @(S,Rbeta) fprintf(...
    ['\t%d discoveries (%d True, %d False)\n' ...
     '\tFDP = %2.2f%% (target FDR = %2.f%%)\n'], ...
    size(S,2), trueDiscoveries(S,Rbeta), size(S,2)-trueDiscoveries(S,Rbeta),100*FDP(S,Rbeta), 100*q);

%% --- Test CVFS.modelSelection --- %

Ntrials = 600;

CVFS_FDP_array   = -ones(1,Ntrials);
CVFS_D_array     = -ones(1,Ntrials);
CVFS_TD_array    = -ones(1,Ntrials);
CVFS_FD_array    = -ones(1,Ntrials);
CVFS_POWER_array = -ones(1,Ntrials);

KF_FDP_array    = -ones(1,Ntrials);
KF_D_array      = -ones(1,Ntrials);
KF_TD_array     = -ones(1,Ntrials);
KF_FD_array     = -ones(1,Ntrials);
KF_POWER_array  = -ones(1,Ntrials);

%                 1       2         3             4                  5
dataset_names = {'null', 'sparse', 'correlated', 'blockcorrelated', 'zouhastie'};
dataset = dataset_names{3};
tag = strcat('CVFS_vs_KF','_',dataset);
debug = 1;

parfor i=1:Ntrials
    [X, y, beta_star] = l1l2.utils.genSynthDataset(dataset, n, p, k, amplitude, sigma);
    
    fname = fullfile(tag,strcat('k',num2str(i)));
    CVFS.utils.write_beta_star(fname, beta_star);  % Save beta star
       
    % Run model selection
    [beta_hat, selected_cvfs, idx_tr, ~] = CVFS.modelSelection(X, y, KF, RLS_fit, RLS_predict, B, test_size, fname, debug);
    fprintf('* CVFS:\n')
    printSummary(selected_cvfs', beta_star)
    CVFS_FDP_array(i) = 100 * FDP(selected_cvfs', beta_star);
    CVFS_D_array(i) = size(selected_cvfs,1);
    CVFS_TD_array(i) = trueDiscoveries(selected_cvfs,beta_star);
    CVFS_FD_array(i) = CVFS_D_array(i) - CVFS_TD_array(i);
    CVFS_POWER_array(i) = power(k,CVFS_TD_array(i));

    % Run kf filter on the same set of samples used for feature selection
    % by CVFS.modelSelection
    selected_kf = knockoff.filter(X, y, q, 'Statistic', @knockoff.stats.lassoSignedMax, ...
                                           'Knockoffs', Knockoffs, 'Threshold', Threshold);
    fprintf('* KF:\n')
    printSummary(selected_kf,beta_star);
    KF_FDP_array(i) = 100 * FDP(selected_kf,beta_star);
    KF_D_array(i) = size(selected_kf,2);
    KF_TD_array(i) = trueDiscoveries(selected_kf,beta_star);
    KF_FD_array(i) = KF_D_array(i) - KF_TD_array(i);
    KF_POWER_array(i) = power(k,KF_TD_array(i));
end
 
% --- results
fprintf('\n\n')
fprintf('*CVFS:\n')
fprintf('\tavg(FDR)   = %2.3f\n',mean(CVFS_FDP_array))
fprintf('\tavg(Power) = %2.3f\n',mean(CVFS_POWER_array))

fprintf('*KF:\n')
fprintf('\tavg(FDR)   = %2.3f\n',mean(KF_FDP_array))
fprintf('\tavg(Power) = %2.3f\n',mean(KF_POWER_array))

% Generate output folder
if ~exist(fullfile('.',tag),'dir')
    mkdir(tag)
end

%% FDP Scatterplot

h1 = figure(1);
plot(1:Ntrials,CVFS_FDP_array,'ob'); hold on
plot(1:Ntrials,KF_FDP_array,'+r')
p1 = plot(1:Ntrials,mean(CVFS_FDP_array)*ones(Ntrials,1),'-b');
p2 = plot(1:Ntrials,mean(KF_FDP_array)*ones(Ntrials,1),'--r');
legend([p1,p2],'CVFS','KF')
title(sprintf('FDP over %d experiments',Ntrials)); hold off
saveas(h1,fullfile(tag,'scatterplot.png'))

%% FDP boxplot

h2 = figure(2);
boxplot([CVFS_FDP_array; KF_FDP_array]','labels',{'CVFS','KF'})
ylim([0 60])
title(sprintf('FDP over %d experiments',Ntrials))
saveas(h2,fullfile(tag,'boxplot.png'))

%% Discoveries barchart

h3 = figure(3);
cvfs = [mean(CVFS_D_array), mean(CVFS_TD_array), mean(CVFS_FD_array)];
kf = [mean(KF_D_array), mean(KF_TD_array), mean(KF_FD_array)];
bar([cvfs; kf],1); hold on
hline = refline([0 k]);
hline.Color = 'r';
hline.LineStyle = '--';
ylim([0,45])
set(gca,'XTickLabel',{'CVFS','KF'})
legend('Discoveries','True D','False D')
title(sprintf('Average discoveries over %d experiments',Ntrials))
saveas(h3,fullfile(tag,'avgDscvr.png'))

%% FDP vs Power scatterplot

h4 = figure(4);
data = [mean(CVFS_FDP_array)/100, mean(KF_FDP_array)/100;
        mean(CVFS_POWER_array), mean(KF_POWER_array)];
gs = gscatter(data(1,:),data(2,:),[1,2],[],'opv');
set(gs, 'MarkerSize', 12)
set(gs(1), 'MarkerFaceColor', 'r')
set(gs(2), 'MarkerFaceColor', 'c')

xlabel('FDP')
ylabel('Power (TD/T)')
legend('CVFS','KF')
axis([0,1,0,1])
saveas(h4,fullfile(tag,'power.png'))