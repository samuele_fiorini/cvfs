function [FS,P] = Kfold(X, y, fsw, K, tag, fitw, predw, debug)
%KFOLD Perform Kfold cross validated feature selection and learning step
%   [FS]   = Kfold(X, y, fsw)
%   [FS]   = Kfold(X, y, fsw, K)
%   [FS]   = Kfold(X, y, fsw, K, tag)
%   [FS,P] = Kfold(X, y, fsw, K, tag, fitw, predw)
%   [FS,P] = Kfold(X, y, fsw, K, tag, fitw, predw, debug)
%
%   This function returns a list of selected features from the
%   input dataset (X,y) performing and l1l2-signature-like (external
%   loop) cross validation procedure. In other words, the dataset
%   is splitted in K chunks and the FS algorithm 'fsw' is
%   iteratively applied on the dataset made of the union of K-1 
%   chunks. The FS algorithm wrapper 'fsw' must return the list
%   of selected features. And it must be consistent with the 
%   following signature.
%
%               [selected] = fsw(X, y)
%   
%   For each selected feature, the correspondent selection frequency 
%   value is returned. If fit and prediction functions ('fitw' and 
%   'predw') are defined, Kfold runs fit & prediction on each split 
%   of the subset of selected features. The fit function handler 'fitw'
%   should take care of internal parameter selection and must satisfy
%   the signature:
%   
%               [b] = fitw(X, y, ...)
%
%   where b are some regression coefficients needed by the selected
%   learning model. The prediction function handler 'predw' must be like:
%
%               [y_pred] = predw(X, b)
%
%   In this case a prediction summary is returned as well.
%
%   Inputs:
%       X     - n x p predictor matrix                        (mandatory)
%       y     - n x 1 response vector                         (mandatory)
%       FSW   - wrapper for the selected FS algorithm         (mandatory)
%       K     - number of splits                              (optional, default = 5)
%       tag   - handy esperiment tag                          (optional)
%       fitw  - wrapper for the selected fitting algorithm    (optional)
%       predw - wrapper for the selected prediction algorithm (optional)
%       debug - 1/0 output on/off                             (optional, default = 0)
%
%   Outputs:
%       FS  - feature selection summary
%       P   - prediction summary
%

% --- arg check --- %
if nargin < 3
    error('CVFS.Kfold: incorrect number of input arguments')
end
if ~exist('K', 'var') || isempty(K)
    K = 5;
end
if ~exist('predw', 'var') || isempty(predw)
    predw = @(X, y) [];
end
if ~exist('fitw', 'var') || isempty(fitw)
    fitw = @(X, y) [];
end
if ~exist('debug', 'var') || isempty(debug)
    debug = 0;
end
if ~exist('tag', 'var') || isempty(tag) % experiment tag
    c = clock;
    tag = strcat('CVFS_summary_',sprintf('%d',c(1)),'-',sprintf('%d',c(2)),'-', ...
        sprintf('%d',c(3)),'_T',sprintf('%d',c(4)),':',sprintf('%d',c(5)),':',sprintf('%2.0f',c(6)));
end

% Generate output folder
if ~exist(tag,'dir') && debug
    mkdir(tag)
end

% --- start function --- %

[n,p] = size(X);
idx = crossvalind('Kfold', n, K);

% -- Kfold loop procedure
P_struct = {}; % prediction performance structure
K_success = 0;
parfor k=1:K
    % Get train - test indices
    test_idx = (idx == k);
    train_idx  = ~test_idx;
    
    % Split dataset
    X_tr = X(train_idx,:);
    y_tr = y(train_idx,:);
    X_ts = X(test_idx,:);
    y_ts = y(test_idx,:);
    
    % Run FS on the training set
    s = fsw(X_tr, y_tr);
    
    % when a subset of features is actually selected
    if ~isempty(s)
        K_success = K_success + 1;
        
        % Run prediction on the subset of selected features
        b = fitw(X_tr(:,s), y_tr);
        
        if ~isempty(b) % if the prediction step is desired
            y_pred_tr = predw(X_tr(:,s), b);
            y_pred_ts = predw(X_ts(:,s), b);
            
            [rss, r2, ~, ~, bacc, prec, rcll, f1, mcc] = CVFS.tools.calc_errors(y_tr, y_ts, y_pred_tr, y_pred_ts);
            P_struct{k} = [k, rss, r2, bacc, prec, rcll, f1, mcc];
        end % calc errors
        
    end % if s
        
    % Save results
    S_struct{k} = s; % this avoids race conditions on S_struct (when this code runs in parallel)
end

% -- Unravel struct
S  = [];
for k=1:K
    S = [S, S_struct{k}];
end
% save the result only when the prediction step had the possibility to
% run, i.e., the selected feature set is not empty
P_struct = P_struct(~cellfun(@isempty, P_struct)); % drop empty cells
P = [];
for k=1:size(P_struct,2)
    if ~isempty(P_struct{k}), P(k,:) = P_struct{k}; end
end

% --- end function --- %

% Check if it's a regression (or a binary classification) problem 
regression_flag = length(unique(y)) > 2;

% Make FS summary
FS = CVFS.utils.make_FS_summary(S,K_success);
  
if debug && K_success > 0
    % Write FS summary on a text file
    CVFS.utils.write_FS_summary(tag, FS, K);

    % Write FS summary on a text file
    CVFS.utils.write_pred_summary(tag, P, regression_flag);
end
    

end
