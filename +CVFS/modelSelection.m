function [beta, selected, idx_tr, idx_ts] = modelSelection(X, y, fsw, fitw, predw,  K, test_size, tag, debug)
%MODELSELECTION Select the best model on a given dataset
%   [beta] = modelSelection(X, y, fsw, fitw, predw)
%   [beta] = modelSelection(X, y, fsw, fitw, predw, K)
%   [beta] = modelSelection(X, y, fsw, fitw, predw, K, tag)
%
%   This function runs CVFS.Kfold on a training split of the input dataset.
%   Then the best list is selected testing its predictive power on a test
%   set. In detail, this function evaluates the prediction error on subset
%   of the test set built, each one, with increasing values of selection
%   frequency.
%
%   Inputs:
%       X         - n x p predictor matrix                        (mandatory)
%       y         - n x 1 response vector                         (mandatory)
%       fsw       - wrapper for the selected FS algorithm         (mandatory)
%       fitw      - wrapper for the selected fitting algorithm    (mandatory)
%       predw     - wrapper for the selected prediction algorithm (mandatory)
%       K         - number of splits                              (optional, default = 5)
%       test_size - in [0,1] fraction of the test set             (default = 0.33)
%       tag       - handy esperiment tag                          (optional)
%       debug     - 1/0 output on/off                             (optional, default = 0)
%
%   Outputs:
%       beta     - coefficients of the selected features
%       selected - index of selected features
%       idx_tr   - sample index used for feature selection
%       idx_ts   - sample index used for testing

% --- arg check --- %
if nargin < 5
    error('CVFS.modelSelection: incorrect number of input arguments')
end
if ~exist('K', 'var') || isempty(K)
    K = 5;
end
if ~exist('test_size', 'var') || isempty(test_size)
    test_size = 0.33;
end
if ~exist('tag', 'var') || isempty(tag) % experiment tag
    c = clock;
    tag = strcat('CVFS_summary_',sprintf('%d',c(1)),'-',sprintf('%d',c(2)),'-', ...
        sprintf('%d',c(3)),'_T',sprintf('%d',c(4)),':',sprintf('%d',c(5)),':',sprintf('%2.0f',c(6)));
end
if ~exist('debug', 'var') || isempty(debug)
    debug = 0;
end

% Generate output folder
if ~exist(tag,'dir') && debug
    mkdir(tag)
end

% --- start function --- %
[n,p] = size(X);

% --- Split dataset --- %
[idx_tr, idx_ts] = CVFS.tools.train_test_split(n, test_size);
X_tr = X(idx_tr,:);
y_tr = y(idx_tr);
X_ts = X(idx_ts,:);
y_ts = y(idx_ts);

% --- Feature selection --- %
[fs,~] = CVFS.Kfold(X_tr, y_tr, fsw, K, tag, fitw, predw, debug);

% --- Model assessment --- %
sel_freq = sort(unique(fs(:,2)),'descend');
K_freq   = size(sel_freq,1); % beware, in case of low SNR size(sel_freq,1) can be lower than K

% Build incremental test set
P_struct = {}; % prediction performance structure
parfor i=1:K_freq
    f = sel_freq(i);               % i-th selection frequency threshold
    idx_f{i} = fs(fs(:,2) >= f,1); % subset of features selected with at least selection frequency f
    
    % Restricting training and test set to the subset of selected features
    X_tr_i  = X_tr(:,idx_f{i}); 
    X_ts_i = X_ts(:,idx_f{i}); 
    
    % Fit & predict
    b{i} = fitw(X_tr_i, y_tr);
    y_pred_tr_i = predw(X_tr_i, b{i});
    y_pred_ts_i = predw(X_ts_i, b{i});
    
    % Prediction performance
    [rss, r2, ~, ~, bacc, prec, rcll, f1, mcc] = CVFS.tools.calc_errors(y_tr, y_ts, y_pred_tr_i, y_pred_ts_i);
    P_struct{i} = [rss, r2,  bacc, prec,  rcll,  f1,    mcc]; 
    %              1 2  3 4  5 6   7 8    9 10   11 12  13 14
end

% Unravel struct
P_struct = P_struct(~cellfun(@isempty, P_struct)); % drop empty cells
P = [];
for k=1:size(P_struct,2)
    if ~isempty(P_struct{k}), P(k,:) = P_struct{k}; end
end

% Select best model from prediction stats
if ~isempty(P)
    % --- Get training and test errors -- %
    tr_rss = P(:,1);
    ts_rss = P(:,2);
    tr_bacc = P(:,5);
    ts_bacc = P(:,6);

    % Select opt features set
    opt_model_idx = find(ts_rss == min(ts_rss));
    selected = idx_f{opt_model_idx};

    % Generate output betas
    beta = zeros(p,1);
    beta(selected) = b{opt_model_idx};

    % Save plots
    if debug
        % Check if it's a regression (or a binary classification) problem 
        regression_flag = length(unique(y)) > 2;

        CVFS.utils.modelSelection_plots(tag, sel_freq, tr_rss, ts_rss, ...
                                        tr_bacc, ts_bacc, regression_flag, opt_model_idx);
    end
else
    beta = zeros(p,1);
    selected = [];
end

end

