function [beta] = CVRLS_fit(X, y, nLambda, nFolds)
%CVRLS_FIT KFold cross validated Regularized Least Squares model fit
%   [beta] = CVRLS_fit(X, y)
%   [beta] = CVRLS_fit(X, y, nLambda)
%   [beta] = CVRLS_fit(X, y, nLambda, nFolds)
%
%   Inputs:
%       X       - n x p predictor matrix
%       y       - n x 1 response vector
%       nLambda - dimension of the L2 parameter grid (optional, default 5*p)
%       nFolds  - number of inner KFCV split (optional, default 5)
%
%   Outputs:
%       beta - p x 1 RLS regressors

% --- arg check --- %
[n,p] = size(X);
if ~exist('nLambda', 'var') || isempty(nLambda)
    nLambda = 5*p;
end
if ~exist('nFolds', 'var') || isempty(nFolds)
    nFolds = 5;
end

% --- start function --- %

% --- L2 penalty range definition --- %
[~,D] = eig(X'*X);
lambda_max = (max(diag(D)) + min(diag(D)))/(2*n); % l1l2-signature like
lambda_min = lambda_max/(2*1e3);
l = fliplr((0:(nLambda-1))/nLambda);
lambda_range = lambda_max .* (lambda_min/lambda_max).^l;

% --- Parameter selection --- %
[n,~] = size(X);
idx = crossvalind('Kfold', n, nFolds);

% -- Kfold loop procedure
vld_err = zeros(nFolds,nLambda);
% tr_err = zeros(nFolds,nLambda);
for k=1:nFolds
    vld_idx = (idx == k);
    train_idx  = ~vld_idx;
    
    % Split dataset
    X_tr = X(train_idx,:);
    y_tr = y(train_idx,:);
    X_vld = X(vld_idx,:);
    y_vld = y(vld_idx,:);
    
    % Fit & predict model
    parfor l=1:nLambda
        beta_l = CVFS.wrapper.RLS_fit(X_tr, y_tr, lambda_range(l)); % RLS solution
        
        y_pred = X_vld*beta_l;
        vld_err(k,l) = CVFS.tools.rss_score(y_vld, y_pred); % validation error
        
%         y_pred_tr = X_tr*beta_l;
%         tr_err(k,l) = CVFS.tools.rss_score(y_tr, y_pred_tr); % validation error
    end
end

% Average error on the nFolds
avg_vld_err = mean(vld_err);
% avg_tr_err = mean(tr_err);

% Find best lambda
lambda_opt = max(lambda_range(avg_vld_err == min(avg_vld_err)));  % get the most regularized solution

% Estimate best betas
beta = CVFS.wrapper.RLS_fit(X,y,lambda_opt);

% %% DEBUG %%%
% figure()
% semilogx(lambda_range, avg_vld_err), hold on
% % semilogx(lambda_range, avg_tr_err)
% semilogx(lambda_opt,avg_vld_err(avg_vld_err == min(avg_vld_err)),'h','MarkerSize',10)
% xlabel('\lambda')
% ylabel('RSS')
% title(sprintf('Average on %d splits',nFolds))
% legend('vld','tr'), hold off
% %% DEBUG %%%

end