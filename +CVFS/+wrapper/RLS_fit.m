function [beta] = RLS_fit(X, y, lambda)
%RLS_FIT Regularized Least Squares model fit
%   [beta] = RLS_fit(X, y, lambda)
%
%   Inputs:
%       X       - n x p predictor matrix
%       y       - n x 1 response vector
%       lambda  - L2 parameter
%
%   Outputs:
%       beta - p x 1 RLS regressors

[n,p] = size(X);

if n < p
    beta = X' * ((X*X' + lambda * n * eye(n)) \ y);
else
    beta = (X'*X + lambda * n * eye(p)) \ (X'*y);
end
