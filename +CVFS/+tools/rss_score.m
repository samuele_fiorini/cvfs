function [rss] = rss_score(y_true, y_pred)
%RSS_SCORE Compute the average residual sum of squares
%   [rss] = rss_score(y_true, y_pred)
%
%   Inputs:
%       y_true - n x 1 training outputs
%       y_pred - n x 1 training predictions
%
%   Outputs:
%       rss - residual sum of squares
%
%   See [https://en.wikipedia.org/wiki/Residual_sum_of_squares]

[n,~] = size(y_pred);
rss = sum((y_pred - y_true).^2)/n;

end

