function [acc] = accuracy_score(y_true, y_pred)
%ACCURACY_SCORE Compute the prediction accuracy
%   [acc] = accuracy_score(y_true, y_pred)
%
%   Inputs:
%       y_true - n x 1 training outputs
%       y_pred - n x 1 training predictions
%
%   Outputs:
%       acc - prediction accuracy
%
%   See [https://en.wikipedia.org/wiki/Confusion_matrix]

% Get sign
y_true = sign(y_true);
y_pred = sign(y_pred);

n = length(y_true);

% Evaluate score
acc = 1 - (sum(y_true ~= y_pred)/n);

end

