function [idx_tr, idx_ts] = train_test_split(n, test_size)
%TRAIN_TEST_SPLIT Split the dataset in training and test
%   [idx_tr, idx_ts] = train_test_split(n)
%   [idx_tr, idx_ts] = train_test_split(n, test_size)
%
%   Inputs:
%       n         - number of samples
%       test_size - in [0,1] fraction of the test set (default = 0.33)
%
%   Outputs:
%       idx_tr - training index
%       idx_ts - test index

if ~exist('test_size', 'var') || isempty(test_size)
    test_size = 0.33;
end

idx = randperm(n);
n_test = round(test_size * n);
idx_ts = idx(1:n_test);
idx_tr = idx(n_test+1:end);

end

