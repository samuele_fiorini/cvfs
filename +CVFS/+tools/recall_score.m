function [rcll] = recall_score(y_true, y_pred)
%RECALL_SCORE Compute the precision score
%   [rcll] = recall_score(y_true, y_pred)
%
%   This function assumes as 'positive' the class having label = +1.
%
%   Inputs:
%       y_true - n x 1 training outputs
%       y_pred - n x 1 training predictions
%
%   Outputs:
%       rcll - recall score
%
%   See [https://en.wikipedia.org/wiki/Confusion_matrix]

% Get sign
y_true = sign(y_true);
y_pred = sign(y_pred);

% Set positive label
positive_label = 1;

% Evaluate the score
idx_pred = find(y_pred == positive_label);
idx_true = find(y_true == positive_label);
TP = sum(y_true(idx_pred) == y_pred(idx_pred)); % the number of TP
FN = sum(y_true(idx_true) ~= y_pred(idx_true)); %the number of FN
rcll = TP / (TP+FN);

if isnan(rcll), rcll = 0; end % prevent from output NaNs
end

