function [RSS, R2, ACC, CHACC, BACC, PREC, RCLL, F1, MCC] = calc_errors(y_tr, y_ts, y_pred_tr, y_pred_ts)
%CALC_ERRORS Returns accuracy and R2 score
%   [clf_err, regr_err] = calc_errors(ytr, yts, y_pred_tr, y_pred_ts)
%
%   Inputs:
%       ytr       - n_tr x 1 training outputs
%       yts       - n_ts x 1 test outputs
%       y_pred_tr - n_tr x 1 predicted training outputs
%       y_pred_ts - n_ts x 1 predicted test outputs
%
%   Outputs:
%       R2    -  [r2_tr,   r2_ts]         where r2_*   is the ouput of CVFS.tools.r2_score(y_true_*, y_pred_*)
%       RSS   -  [r2_tr,   r2_ts]         where r2_*   is the ouput of CVFS.tools.rss_score(y_true_*, y_pred_*)
%       ACC   -  [acc_tr,  acc_ts]        where acc_*  is the ouput of CVFS.tools.accuracy_score(y_true_*, y_pred_*)
%       CHACC -  [ch_acc_tr,  ch_acc_ts]  where ch_acc_*  is the ouput of CVFS.tools.chance_accuracy_score(y_true_*, y_pred_*)
%       PREC  -  [prec_tr, prec_ts]       where prec_* is the ouput of CVFS.tools.precision_score(y_true_*, y_pred_*)
%       RCLL  -  [rcll_tr, rcll_ts]       where rcll_* is the ouput of CVFS.tools.recall_score(y_true_*, y_pred_*)
%       F1    -  [f1_tr,   f1_ts]         where f1_*   is the ouput of CVFS.tools.f1_score(y_true_*, y_pred_*)
%       MCC   -  [mcc_tr,  mcc_ts]        where mcc_*  is the ouput of CVFS.tools.mcc_score(y_true_*, y_pred_*)

%  ---   Regression   --- %
rss_tr = CVFS.tools.rss_score(y_tr, y_pred_tr);
rss_ts = CVFS.tools.rss_score(y_ts, y_pred_ts);
RSS = [rss_tr, rss_ts];

r2_tr = CVFS.tools.r2_score(y_tr, y_pred_tr);
r2_ts = CVFS.tools.r2_score(y_ts, y_pred_ts);
R2 = [r2_tr, r2_ts]; 


%  ---  Classification --- %
acc_tr = CVFS.tools.accuracy_score(y_tr, y_pred_tr);
acc_ts = CVFS.tools.accuracy_score(y_ts, y_pred_ts);
ACC = [acc_tr, acc_ts];    

ch_acc_tr = CVFS.tools.chance_accuracy_score(y_tr);
ch_acc_ts = CVFS.tools.chance_accuracy_score(y_ts);
CHACC = [ch_acc_tr, ch_acc_ts];    

bacc_tr = CVFS.tools.balanced_accuracy_score(y_tr, y_pred_tr);
bacc_ts = CVFS.tools.balanced_accuracy_score(y_ts, y_pred_ts);
BACC = [bacc_tr, bacc_ts];    

prec_tr = CVFS.tools.precision_score(y_tr, y_pred_tr);
prec_ts = CVFS.tools.precision_score(y_ts, y_pred_ts);
PREC = [prec_tr, prec_ts];    

rcll_tr = CVFS.tools.recall_score(y_tr, y_pred_tr);
rcll_ts = CVFS.tools.recall_score(y_ts, y_pred_ts);
RCLL = [rcll_tr, rcll_ts];    

f1_tr = CVFS.tools.f1_score(y_tr, y_pred_tr);
f1_ts = CVFS.tools.f1_score(y_ts, y_pred_ts);
F1 = [f1_tr, f1_ts];    

mcc_tr = CVFS.tools.mcc_score(y_tr, y_pred_tr);
mcc_ts = CVFS.tools.mcc_score(y_ts, y_pred_ts);
MCC = [mcc_tr, mcc_ts];    

end

