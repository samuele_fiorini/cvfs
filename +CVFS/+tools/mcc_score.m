function [mcc] = mcc_score(y_true, y_pred)
%MCC_SCORE Compute the Matthews correlation coefficient
%   [mcc] = mcc_score(y_true, y_pred)
%
%   This function assumes as 'positive' the class having label = +1.
%
%   Inputs:
%       y_true - n x 1 training outputs
%       y_pred - n x 1 training predictions
%
%   Outputs:
%       mcc - Matthews correlation coefficient
%
%   See [https://en.wikipedia.org/wiki/Confusion_matrix]

% Get sign
y_true = sign(y_true);
y_pred = sign(y_pred);

% Set positive label
positive_label = 1;

% Evaluate the score
idx_pred   = find(y_pred == positive_label);
idx_pred_N = find(y_pred ~= positive_label);
idx_true = find(y_true == positive_label);
TP = sum(y_true(idx_pred) == y_pred(idx_pred)); % the number of TP
TN = sum(y_true(idx_pred_N) == y_pred(idx_pred_N)); % the number of TN
FP = sum(y_true(idx_pred) ~= y_pred(idx_pred)); % the number of FP
FN = sum(y_true(idx_true) ~= y_pred(idx_true)); %the number of FN

mcc = ((TP*TN) - (FP*FN)) / sqrt((TP+FP) * (TP+FN) * (TN+FP) * (TN+FN));

if isnan(mcc), mcc = 0; end % prevent from output NaNs
end

