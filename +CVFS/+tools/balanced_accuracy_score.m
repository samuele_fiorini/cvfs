function [bacc] = balanced_accuracy_score(y_true, y_pred)
%BALANCED_ACCURACY_SCORE Compute the balanced prediction accuracy
%   [bacc] = balanced_accuracy_score(y_true, y_pred)
%
%   Inputs:
%       y_true - n x 1 training outputs
%       y_pred - n x 1 training predictions
%
%   Outputs:
%       bacc - balanced prediction accuracy
%
%   See [https://en.wikipedia.org/wiki/Accuracy_and_precision]

% Get sign
y_true = sign(y_true);
y_pred = sign(y_pred);

% Evaluate score
n = length(y_true);
err = sum((y_true ~= y_pred) .* abs(y_true - mean(y_true))) / n;

bacc = 1 - err;
end

