function [chance] = chance_accuracy_score(y)
%CHANCE_ACCURACY_SCORE Compute the prediction accuracy of the flipcoin
%   [chance] = chance_accuracy_score(y_true, y_pred)
%
%   Inputs:
%       y - n x 1 outputs
%
%   Outputs:
%       chance - prediction accuracy of the flipcoin classifier
%

n = size(y,1);
chance = max([sum(sign(y) == 1), sum(sign(y) == -1)])/n;

end

