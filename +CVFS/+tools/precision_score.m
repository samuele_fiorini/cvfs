function [prec] = precision_score(y_true, y_pred)
%PRECISION_SCORE Compute the precision score
%   [prec] = precision_score(y_true, y_pred)
%
%   This function assumes as 'positive' the class having label = +1.
%
%   Inputs:
%       y_true - n x 1 training outputs
%       y_pred - n x 1 training predictions
%
%   Outputs:
%       prec - precision score
%
%   See [https://en.wikipedia.org/wiki/Confusion_matrix]

% Get sign
y_true = sign(y_true);
y_pred = sign(y_pred);

% Set positive label
positive_label = 1;

% Evaluate the score
idx_pred = find(y_pred == positive_label);
TP = sum(y_true(idx_pred) == y_pred(idx_pred)); % the number of TP
FP = sum(y_true(idx_pred) ~= y_pred(idx_pred)); % the number of FP
prec = TP / (TP+FP);

if isnan(prec), prec = 0; end % prevent from output NaNs
end

