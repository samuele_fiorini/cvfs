function [f1] = f1_score(y_true, y_pred)
%F1_SCORE Compute the f1 score
%   [f1] = f1_score(y_true, y_pred)
%
%   This function assumes as 'positive' the class having label = +1.
%
%   Inputs:
%       y_true - n x 1 training outputs
%       y_pred - n x 1 training predictions
%
%   Outputs:
%       f1 - f1 score
%
%   See [https://en.wikipedia.org/wiki/Confusion_matrix]

prec = CVFS.tools.precision_score(y_true, y_pred);
rcll = CVFS.tools.recall_score(y_true, y_pred);
f1 = harmmean([prec, rcll]);

end
