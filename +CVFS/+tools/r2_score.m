function [r2] = r2_score(y_true, y_pred)
%R2_SCORE Compute the coefficient of determination on given inputs
%   [r2] = r2_score(y_true, y_pred)
%
%   Inputs:
%       y_true - n x 1 training outputs
%       y_pred - n x 1 training predictions
%
%   Outputs:
%       r2 - coefficient of determination
%
%   See [https://en.wikipedia.org/wiki/Coefficient_of_determination]

n = length(y_true);
SStot = var(y_true)*(n-1); % i.e. sum((y_true - mean(y_true)).^2);
SSres = sum((y_pred - y_true).^2);
r2 = (1 - (SSres / SStot));

end

