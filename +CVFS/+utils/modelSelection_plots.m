function [fileName] = modelSelection_plots(tag, sel_freq, tr_rss, ts_rss, tr_bacc, ts_bacc, regression_flag, opt_idx)
%MODEL_SELECTION_PLOTS Generate intuitive feature selection plots
%   [fileName] = modelSelection_plot(tag,  tr_rss, ts_rss, tr_acc, ts_acc)
%
%   Inputs:
%       tag       - experiment tag in file name
%       sel_freq  - selection frequency (i.e. the x axis)
%       tr_rss    - training regression error
%       ts_rss    - test regression error
%       tr_bacc   - training accuracy
%       ts_bacc   - test accuracy
%       regression_flag - 1/0 regression/classification
%       opt_idx   - best model index (optional)
%
if ~exist('opt_idx', 'var') || isempty(opt_idx)
    opt_idx = find(ts_rss == min(ts_rss));
end


% RSS
h1 = figure('Visible','off');
plot(sel_freq, tr_rss, '-o'), hold on
plot(sel_freq, ts_rss, '-o')
plot(sel_freq(opt_idx), ts_rss(opt_idx),'h','MarkerSize',10)
xlabel('Selection frequency')
ylabel('RSS')
title('RSS on decreasing test subsets')
legend('tr','ts'), hold off
saveas(h1, fullfile(tag,'modelSelection_RSS'),'png')

% BALANCED ACC
if ~regression_flag
    h2 = figure('Visible','off');
    plot(sel_freq, tr_bacc, '-o'), hold on
    plot(sel_freq, ts_bacc, '-o')
    ylim([0,1])
    xlabel('Selection frequency')
    ylabel('BALANCED ACC')
    title('Balanced accuracy score on decreasing test subsets')
    legend('tr','ts'), hold off
    saveas(h2, fullfile(tag,'modelSelection_BACC'),'png')
end

% close(h1,h2)
end

