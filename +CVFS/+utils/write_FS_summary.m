function [fileName] = write_FS_summary(tag, summary, K)
%WRITE_FS_SUMMARY Generate text file with FS summary
%   [fileName] = write_FS_summary(tag, summary)
%
%   Inputs:
%       summary - n_sel x 4 matrix presenting, on each row: 
%                 [feature index, #counts/#folds, #counts, #folds]
%       tag     - experiment tag in file name
%       K       - original number of CV folds

% Generate output folder
if ~exist(tag,'dir')
    mkdir(tag)
end

fileName = fullfile(tag,strcat('CVFS_summary_','_features.txt'));
fid = fopen(fileName,'w');

header = 'Idx\t\tfreq\t\tnum sel\n';

fprintf(fid,'Original number of CV splits: %d\n',K);
fprintf(fid,strcat(repmat('-',1,2*length(header)),'\n'));
fprintf(fid,header);
fprintf(fid,strcat(repmat('-',1,2*length(header)),'\n'));
fprintf(fid,'%d\t\t%2.2f\t\t%2.0f/%d\n',summary');

end

