function [fileName] = write_pred_summary(tag, summary, regression_flag)
%WRITE_PRED_SUMMARY Generate text file with prediction summary
%   [fileName] = write_FS_summary(tag, summary)
%
%   Inputs:
%       summary         - n_splits x 7 matrix presenting, on each row: 
%                         [RSS, R2, BACC, PREC, RCLL, F1, MCC]
%       tag             - experiment tag in file name
%       regression_flag - 1/0 regression/classification

% Generate output folder
if ~exist(tag,'dir')
    mkdir(tag)
end

% Split training and test summary
split    = summary(:,1);
summary  = summary(:,2:end);
training = [split, summary(:,1:2:end)];
test     = [split, summary(:,2:2:end)];

% Get file id
fileName = fullfile(tag,strcat('CVFS_summary_','_prediction.txt'));
fid_list = [1, fopen(fileName,'w')];


% print results on both tty and file
for i=1:size(fid_list,2)
    fid = fid_list(i);

    % Print summary according to regression_flag
    if regression_flag

        % Define header
        header = sprintf('SPLIT\t\tRSS\t\tR2\n');
        
        fprintf(fid,'Training stats\n');
        fprintf(fid,strcat(repmat('-',1,3*length(header)),'\n'));
        fprintf(fid,header);
        fprintf(fid,strcat(repmat('-',1,3*length(header)),'\n'));
        fprintf(fid,'%1.0f\t\t%2.2f\t\t%2.2f\n',training(:,1:3)');
        fprintf(fid,strcat(repmat('-',1,3*length(header)),'\n'));
        fprintf(fid,'Mean\t\t%2.2f\t\t%2.2f\n',mean(training(:,2:3),1));
        fprintf(fid,'Std\t\t%2.2f\t\t%2.2f\n\n',std(training(:,2:3),1));

        fprintf(fid,'Test stats\n');
        fprintf(fid,strcat(repmat('-',1,3*length(header)),'\n'));
        fprintf(fid,header);
        fprintf(fid,strcat(repmat('-',1,3*length(header)),'\n'));
        fprintf(fid,'%1.0f\t\t%2.2f\t\t%2.2f\n',test(:,1:3)');
        fprintf(fid,strcat(repmat('-',1,3*length(header)),'\n'));
        fprintf(fid,'Mean\t\t%2.2f\t\t%2.2f\n',mean(test(:,2:3),1));
        fprintf(fid,'Std\t\t%2.2f\t\t%2.2f\n',std(test(:,2:3),1));

    else
        
        % Define header
        header = sprintf('SPLIT\t\tBACC\t\tPREC\t\tRCLL\t\tF1\t\tMCC\n');
        
        fprintf(fid,'Training stats\n');
        fprintf(fid,strcat(repmat('-',1,3*length(header)),'\n'));
        fprintf(fid,header);
        fprintf(fid,strcat(repmat('-',1,3*length(header)),'\n'));
        fprintf(fid,'%1.0f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\n',[training(:,1), training(:,4:end)]');
        fprintf(fid,strcat(repmat('-',1,3*length(header)),'\n'));
        fprintf(fid,'Mean\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\n',mean(training(:,4:end),1));
        fprintf(fid,'Std\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\n\n',std(training(:,4:end),1));

        fprintf(fid,'Test stats\n');
        fprintf(fid,strcat(repmat('-',1,3*length(header)),'\n'));
        fprintf(fid,header);
        fprintf(fid,strcat(repmat('-',1,3*length(header)),'\n'));
        fprintf(fid,'%1.0f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\n',[test(:,1), test(:,4:end)]');
        fprintf(fid,strcat(repmat('-',1,3*length(header)),'\n'));
        fprintf(fid,'Mean\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\n',mean(test(:,4:end),1));
        fprintf(fid,'Std\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\n',std(test(:,4:end),1));

    end

end % for file/tty
% 
% 
% % print results on both tty and file
% for i=1:size(fid_list,2)
%     fid = fid_list(i);
%     
%     fprintf(fid,'Training stats\n');
%     fprintf(fid,strcat(repmat('-',1,3*length(header)),'\n'));
%     fprintf(fid,header);
%     fprintf(fid,strcat(repmat('-',1,3*length(header)),'\n'));
%     fprintf(fid,'%1.0f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\n',training');
%     fprintf(fid,strcat(repmat('-',1,3*length(header)),'\n'));
%     fprintf(fid,'Mean\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\n',mean(training(:,2:end),1));
%     fprintf(fid,'Std\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\n\n',std(training(:,2:end),1));
%     
%     fprintf(fid,'Test stats\n');
%     fprintf(fid,strcat(repmat('-',1,3*length(header)),'\n'));
%     fprintf(fid,header);
%     fprintf(fid,strcat(repmat('-',1,3*length(header)),'\n'));
%     fprintf(fid,'%1.0f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\n',test');
%     fprintf(fid,strcat(repmat('-',1,3*length(header)),'\n'));
%     fprintf(fid,'Mean\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\n',mean(test(:,2:end),1));
%     fprintf(fid,'Std\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\t\t%2.2f\n',std(test(:,2:end),1));
% end
% 
% fclose(fid_list(2));
% 
end