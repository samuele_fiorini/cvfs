function [fileName] = write_beta_star(tag, beta_star)
%WRITE_BETA_STAR Generate text file with beta star
%   [fileName] = write_beta_star(tag, beta_star)
%
%   Inputs:
%       beta_star - n x 1 vector of real beta
%       tag     - experiment tag in file name

% Generate output folder
if ~exist(tag,'dir')
    mkdir(tag)
end

fileName = fullfile(tag,'CVFS_beta_star.txt');
fid = fopen(fileName,'w');

header = 'Beta star indexes\n';
fprintf(fid,header);
fprintf(fid,strcat(repmat('-',1,length(header)),'\n'));
fprintf(fid,'%d\n',find(abs(beta_star)>0)');

end

