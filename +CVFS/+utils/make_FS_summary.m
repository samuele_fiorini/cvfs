function [summary] = make_FS_summary(S,K)
%MAKE_FS_SUMMARY Generate text file with FS summary
%   [fileName] = write_FS_summary(tag, summary)
%
%   Inputs:
%       S - array of selected features
%       K - number of CV folds
%
%   Outputs:
%       summary - n_sel x 4 matrix presenting, on each row: 
%                 [feature index, #counts/#folds, #counts, #folds]
       
selected = unique(S); % List of features selected at least one time
summary = zeros(length(selected),4);
for i=1:length(selected)
    counts = sum(S == selected(i));
    summary(i,:) = [selected(i), counts/K, counts, K];
end
summary = flipud(sortrows(summary,3)); % Sort in descending order wrt selection frequency

end
